package com.aeontronix.enhancedmule.config;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = EMCredentialsBearerTokenImpl.class, name = "bearer"),
})
public interface EMCredentials {
}
